//
//  AppDelegate.h
//  iOSTemplete
//
//  Created by Maktub on 01/05/2017.
//  Copyright © 2017 maktub. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

