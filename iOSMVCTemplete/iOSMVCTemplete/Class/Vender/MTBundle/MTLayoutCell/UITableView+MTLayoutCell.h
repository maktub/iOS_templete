//
//  UITableView+MTLayoutCell.h
//  onemoney
//
//  Created by Maktub on 16/3/14.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (MTLayoutCell)

- (CGFloat)mt_heightForCellWithIdentifier:(NSString *)identifier configuration:(void (^)(id cell))configuration;

@end
