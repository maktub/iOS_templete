//
//  UITableView+MTLayoutCell.m
//  onemoney
//
//  Created by Maktub on 16/3/14.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import "UITableView+MTLayoutCell.h"

#import "UITableView+FDTemplateLayoutCell.h"

@implementation UITableView (MTLayoutCell)

- (CGFloat)mt_heightForCellWithIdentifier:(NSString *)identifier configuration:(void (^)(id cell))configuration{
    return [self fd_heightForCellWithIdentifier:identifier configuration:^(id cell) {
        if (configuration) {
            configuration(cell);
        }
    }];
}
@end
