//
//  UIView+MTLayoutToFrame.h
//  onemoney
//
//  Created by Maktub on 16/3/15.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (MTLayoutToFrame)

- (instancetype)mt_layoutToFrame;

@end
