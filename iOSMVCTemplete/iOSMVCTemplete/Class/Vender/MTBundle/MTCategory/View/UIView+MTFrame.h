//
//  UIView+MTFrame.h
//  YingCaa
//
//  Created by interest on 16/4/26.
//  Copyright © 2016年 interest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (MTFrame)

//宽度
@property (nonatomic, assign) CGFloat mt_width;

//高度
@property (nonatomic, assign) CGFloat mt_height;

//坐标 x
@property (nonatomic, assign) CGFloat mt_x;

//坐标 y
@property (nonatomic, assign) CGFloat mt_y;

//屏幕大小
@property (nonatomic, assign) CGSize mt_size;

@end
