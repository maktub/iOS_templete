//
//  UITextField+MTLeftRight.h
//  onemoney
//
//  Created by Maktub on 16/3/21.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (MTLeftRight)

- (void)mt_setLeftViewWithImage:(UIImage *)image;
- (void)mt_setLeftViewWithSearchImage:(UIImage *)image;
@end
