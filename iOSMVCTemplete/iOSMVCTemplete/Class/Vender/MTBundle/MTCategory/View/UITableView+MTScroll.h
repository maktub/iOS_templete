//
//  UITableView+MTScroll.h
//  onemoney
//
//  Created by Maktub on 16/3/28.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (MTScroll)

- (void)mt_scrollToTop ;

- (void)mt_scrollToBottom;

@end
