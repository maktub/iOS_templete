//
//  UIView+Vc.m
//  onemoney
//
//  Created by Maktub on 16/1/12.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import "UIView+MTGetVc.h"


@implementation UIView (MTGetVc)

- (UIViewController *)mt_getCurrentController:(Class)Vc{
    UIResponder *responder = self;
    while ((responder = [responder nextResponder]))
        if ([responder isKindOfClass: Vc])
            return (UIViewController *)responder;
    return nil;
}


@end
