//
//  UIView+MTLayoutToFrame.m
//  onemoney
//
//  Created by Maktub on 16/3/15.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import "UIView+MTLayoutToFrame.h"

@implementation UIView (MTLayoutToFrame)

- (instancetype)mt_layoutToFrame{
    CGSize size = [self systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
    return self;
}

@end
