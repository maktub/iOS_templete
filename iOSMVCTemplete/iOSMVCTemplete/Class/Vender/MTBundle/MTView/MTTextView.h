//
//  MTTextView.h
//  iOSTemplete
//
//  Created by Maktub on 13/07/2017.
//  Copyright © 2017 maktub. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTTextView : UITextView

@property (copy, nonatomic, nullable)  NSString *mt_placeholder;

@property (strong, nonatomic, nullable)  UIColor *mt_placeholderColor;

@property (strong, nonatomic, nullable) UIFont *mt_placeholderFont;
@end
