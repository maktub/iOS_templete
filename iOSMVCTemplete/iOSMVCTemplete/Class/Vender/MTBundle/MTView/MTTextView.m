//
//  MTTextView.m
//  iOSTemplete
//
//  Created by Maktub on 13/07/2017.
//  Copyright © 2017 maktub. All rights reserved.
//

#import "MTTextView.h"

@interface MTTextView ()

@property (weak, nonatomic) UILabel *placeholderLabel;

@end

@implementation MTTextView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (!self) return nil;
    [self setUp];
    return self;
}

- (void)setUp {
    
    UILabel *placeholderLabel = [[UILabel alloc] init];
    placeholderLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:_placeholderLabel = placeholderLabel];
    
    self.mt_placeholderColor = [UIColor lightGrayColor];
    self.mt_placeholderFont = [UIFont systemFontOfSize:16.0f];
    self.font = [UIFont systemFontOfSize:16.0f];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange) name:UITextViewTextDidChangeNotification object:self];
}

#pragma mark - UITextViewTextDidChangeNotification

- (void)textDidChange {
    self.placeholderLabel.hidden = self.hasText;
}


- (void)setText:(NSString *)text {
    [super setText:text];
    
    [self textDidChange];
}

- (void)setAttributedText:(NSAttributedString *)attributedText {
    [super setAttributedText:attributedText];
    
    [self textDidChange];
}


- (void)setMt_placeholderFont:(UIFont *)mt_placeholderFont {
    _mt_placeholderFont = mt_placeholderFont;
    self.placeholderLabel.font = mt_placeholderFont;
    [self setNeedsLayout];
}

- (void)setMt_placeholder:(NSString *)mt_placeholder {
    _mt_placeholder = [mt_placeholder copy];
    
    self.placeholderLabel.text = mt_placeholder;
    
    [self setNeedsLayout];
    
}


- (void)setMt_placeholderColor:(UIColor *)mt_placeholderColor {
    _mt_placeholderColor = mt_placeholderColor;
    self.placeholderLabel.textColor = mt_placeholderColor;
}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect frame = self.placeholderLabel.frame;
    frame.origin.y = self.textContainerInset.top;
    frame.origin.x = self.textContainerInset.left+6.0f;
    frame.size.width = self.frame.size.width - self.textContainerInset.left*2.0;
    
    CGSize maxSize = CGSizeMake(frame.size.width, MAXFLOAT);
    frame.size.height = [self.mt_placeholder boundingRectWithSize:maxSize options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : self.placeholderLabel.font} context:nil].size.height;
    self.placeholderLabel.frame = frame;
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:UITextViewTextDidChangeNotification];
}

@end
