//
//  MTSwithView.h
//  iOSTemplete
//
//  Created by Maktub on 13/07/2017.
//  Copyright © 2017 maktub. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MTSwithView : UIView
-(instancetype _Nullable )initWithArray:(NSArray *_Nullable)titleArray;

- (void)addTarget:(nullable id)target action:(SEL _Nullable )action;

@property (nonatomic, assign) NSInteger index;

@property (nonatomic, assign) BOOL reload;

@end
