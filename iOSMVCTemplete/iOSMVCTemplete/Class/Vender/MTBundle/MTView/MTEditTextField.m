//
//  MTEditTextField.m
//  iOSTemplete
//
//  Created by Maktub on 2017/12/10.
//  Copyright © 2017年 maktub. All rights reserved.
//

#import "MTEditTextField.h"

@interface MTEditTextField()

@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation MTEditTextField

-(instancetype)init{
    if (self = [super init]) {
        self.backgroundColor = [UIColor whiteColor];
        UILabel *titleLabel = [[UILabel alloc] init];
//        titleLabel.font = ELMainFont;
//        titleLabel.textColor = ELFontColor;
        titleLabel.text = @"标题:";
        [titleLabel sizeToFit];
        _titleLabel = titleLabel;
        [self addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self);
            make.left.equalTo(self).offset(10);
        }];
        
        UITextField *textField = [[UITextField alloc] init];
        _textField = textField;
        [self addSubview:textField];
        [textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.top.equalTo(self);
            make.left.equalTo(titleLabel.mas_right).offset(5);
            make.right.equalTo(self).offset(-10);
        }];
        
        
    }
    return self;
}

- (void)setTitle:(NSString *)text font:(UIFont *)font{
        _titleLabel.text = text;
    CGSize size = CGSizeMake(100, MAXFLOAT);//设置高度宽度的最大限度
    CGRect rect = [text boundingRectWithSize:size options:NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : font} context:nil];
    

    
    // ceilf()向上取整函数, 只要大于1就取整数2. floor()向下取整函数, 只要小于2就取整数1.
//    CGSize size2 = CGSizeMake(ceilf(size.width), ceilf(size.height));
    [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self);
        make.left.equalTo(self).offset(10);
        make.width.equalTo(@(rect.size.width*1.1));
    }];
    [self layoutIfNeeded];
}


@end
