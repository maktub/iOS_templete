//
//  MTView.h
//  iOSTemplete
//
//  Created by Maktub on 07/07/2017.
//  Copyright © 2017 maktub. All rights reserved.
//

#ifndef MTView_h
#define MTView_h

#import "MTButton.h"
#import "MTSwithView.h"
#import "MTTextView.h"
#import "MTImageView.h"
#import "MTEditTextField.h"
#endif /* MTView_h */
