//
//  UIScrollView+MTRefresh.h
//  onemoney
//
//  Created by Maktub on 16/3/14.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (MTRefresh)

- (void)mt_headerRefreshWithBlock:(void (^)(void))block;
- (void)mt_headerRefreshWithContentInsetTop:(CGFloat)num Block:(void (^)(void))block;
- (void)mt_footerRefreshWithBlock:(void (^)(void))block;
- (void)mt_footerRefreshWithBlock:(void (^)(void))block ContentInsetBottom:(CGFloat)num;
- (void)mt_endRefreshing;
- (void)mt_setNoMoreData;
- (void)mt_resetNoMoreData;
-(void)mt_footerBeginRefreshing;
-(void)mt_headerBeginRefreshing;

@end
