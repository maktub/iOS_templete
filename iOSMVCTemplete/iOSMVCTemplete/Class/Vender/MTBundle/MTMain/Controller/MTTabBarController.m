//
//  MTTabBarController.m
//  YingCaa
//
//  Created by interest on 16/4/26.
//  Copyright © 2016年 interest. All rights reserved.
//

#import "MTTabBarController.h"

//controller
#import "MTNavigationController.h"

@interface MTTabBarController ()

@property (nonatomic, strong) NSMutableArray<UITabBarItem *> *vcArray;

@end

@implementation MTTabBarController

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(NSMutableArray<UITabBarItem *> *)vcArray{
    if (nil == _vcArray) {
        _vcArray = [NSMutableArray array];
    }
    return _vcArray;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    /**
     *   设置tabbar样式
     */
    [self setUpTabBar];
    
    /**
     *  添加子控制器
     */
    [self setUpAllChildViewController];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//初始化控件属性
-(void)setUpTabBar{

//    self.tabBar.backgroundImage = [[UIImage alloc] init];
//    self.tabBar.shadowImage = [[UIImage alloc] init];
//    self.tabBar.barTintColor = [UIColor whiteColor];
//    self.tabBar.translucent = NO;
}


//初始化子控件
-(void)setUpAllChildViewController{
    //创建子控件
    //首页
    UIViewController *homeVc = [[UIViewController alloc] init];
    [self setUpChildViewController:homeVc image:[UIImage imageNamed:@"首页"] selectedImage:[UIImage imageNamed:@"首页-点击"] title:@"首页"];
    //管家
    UIViewController *keeperVc = [[UIViewController alloc] init];
    [self setUpChildViewController:keeperVc image:[UIImage imageNamed:@"管家"] selectedImage:[UIImage imageNamed:@"管家-点击"] title:@"管家"];
    
    //商城
    UIViewController *shopVc = [[UIViewController alloc] init];
    [self setUpChildViewController:shopVc image:[UIImage imageNamed:@"商城"] selectedImage:[UIImage imageNamed:@"商城-点击"] title:@"商城"];
    
    //我的
    UIViewController *personalVc = [[UIViewController alloc] init];
    [self setUpChildViewController:personalVc image:[UIImage imageNamed:@"我的"] selectedImage:[UIImage imageNamed:@"我的-点击"] title:@"我的"];
}
//设置子控件
-(void)setUpChildViewController:(UIViewController *)vc
                          image:(UIImage *)image
                  selectedImage:(UIImage *)selectedImage
                          title:(NSString *)title
{
    //设置标题
    vc.title = title;
    //设置字体颜色
//    [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:YCMainColor} forState:UIControlStateNormal];
   
    //设置图片并关闭默认颜色
    vc.tabBarItem.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    vc.tabBarItem.selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1]} forState:UIControlStateNormal];
    [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:61/255.0 green:167/255.0 blue:247/255.0 alpha:1]} forState:UIControlStateSelected];
    //添加NavigationController
    MTNavigationController *navVc = [[MTNavigationController alloc] initWithRootViewController:vc];
//    [navVc setNavigationBarHidden:YES animated:YES];
    [self addChildViewController:navVc];
    
}


@end
