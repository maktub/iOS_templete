//
//  MTUploadParam.h
//  onemoney
//
//  Created by Maktub on 15/12/22.
//  Copyright © 2015年 Maktub-小明. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MTUploadParam : NSObject
@property (nonatomic, strong) NSData *data;
@property (nonatomic, copy) NSString *fileName;
@property (nonatomic, copy) NSString *paramName;
@property (nonatomic, copy) NSString *mineType;
@end
