//
//  MTHttpTool.m
//  onemoney
//
//  Created by Maktub on 15/12/21.
//  Copyright © 2015年 Maktub-小明. All rights reserved.
//

#import "MTHttpTool.h"
#import "NSObject+MTKeyValue.h"
#import "AFNetWorking.h"


@implementation MTHttpTool

+ (void)post:(NSString *)URLString
  parameters:(NSObject *)parameters
     success:(void (^)(id responseObject))success
     failure:(void (^)(NSError *error))failure{
   
    AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];

    mgr.requestSerializer.timeoutInterval = 30;
    mgr.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",@"text/plain",nil];
    [mgr POST:URLString parameters:parameters.mt_keyValues progress:^(NSProgress * _Nonnull uploadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(responseObject);
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
    
}

+ (void)get:(NSString *)URLString
  parameters:(NSObject *)parameters
     success:(void (^)(id responseObject))success
     failure:(void (^)(NSError *error))failure{
    AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
    mgr.requestSerializer.timeoutInterval = 30;
        mgr.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",@"text/plain",nil];
    [mgr GET:URLString parameters:parameters.mt_keyValues progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (success) {
            success(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failure) {
            failure(error);
        }
    }];
}

+ (void)upload:(NSString *)URLString
    parameters:(NSObject *)parameters
   uploadParam:(NSArray<MTUploadParam *>*)uploadParam
       success:(void (^)(id responseObject))success
       failure:(void (^)(NSError *))failure{
    // 创建请求管理者
    AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
    mgr.requestSerializer.timeoutInterval = 30;
            mgr.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",@"text/plain",nil];
    // 发送请求
    [mgr POST:URLString parameters:parameters.mt_keyValues constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if (uploadParam != nil) {
            [uploadParam enumerateObjectsUsingBlock:^(MTUploadParam * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            [formData appendPartWithFileData:obj.data name:obj.paramName fileName:obj.fileName mimeType:obj.mineType];
            }];

        }
  
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        success(responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        failure(error);
        
    }];
    
    
}
@end
