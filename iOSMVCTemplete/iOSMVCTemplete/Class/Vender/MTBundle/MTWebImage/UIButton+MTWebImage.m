//
//  UIButton+MTWebImage.m
//  onemoney
//
//  Created by Maktub on 16/3/5.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import "UIButton+MTWebImage.h"
#import "UIButton+WebCache.h"

@implementation UIButton (MTWebImage)

- (void)mt_setBackgroundImageWithURL:(NSString *)urlstring forState:(UIControlState)state{
    [self sd_setBackgroundImageWithURL:[NSURL URLWithString:urlstring] forState:(UIControlState)state];
}

- (void)mt_setImageWithURL:(NSString *)urlstring forState:(UIControlState)state{
    [self sd_setImageWithURL:[NSURL URLWithString:urlstring] forState:(UIControlState)state];
}



@end
