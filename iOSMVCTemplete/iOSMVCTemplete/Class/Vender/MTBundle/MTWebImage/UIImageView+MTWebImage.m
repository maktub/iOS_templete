//
//  UIImageView+MTWebImage.m
//  onemoney
//
//  Created by Maktub on 16/3/3.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import "UIImageView+MTWebImage.h"
#import "UIImageView+WebCache.h"

//ftp
//#import <FTPManager/FTPManager.h>

@implementation UIImageView (MTWebImage)

-(void)mt_setImageWithUrlString:(NSString *)urlstring{
    [self sd_setImageWithURL:[NSURL URLWithString:urlstring]];
}

-(void)mt_setRetSetImageWithUrlString:(NSString *)urlstring{
    [self sd_setImageWithURL:[NSURL URLWithString:urlstring] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
//        NSLog(@"%@",image.size);
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(MTScreenWidth));
            make.height.equalTo(self.mas_width).dividedBy(image.size.width/image.size.height);
        }];
    }];
}

-(void)mt_setImageWithUrlString:(NSString *)urlstring fileName:(NSString *)fileName{
    
}

//-(void)mt_setImageWithFtp:(NSString *)urlstring fileName:(NSString *)fileName{
//    
//    NSString *caches = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
//    NSString *pic = [NSString stringWithFormat:@"MtPicCaches/%@",fileName];
//    NSString *picPath = [caches stringByAppendingPathComponent:pic];
//    NSFileManager *mgr = [NSFileManager defaultManager];
//    if ([mgr fileExistsAtPath:picPath]) {
//        self.image = [UIImage imageWithContentsOfFile:picPath];
//        return;
//    }else{
//        //创建文件夹
//        [mgr createDirectoryAtPath:[caches stringByAppendingPathComponent:@"MtPicCaches"]  withIntermediateDirectories:YES attributes:nil error:nil];
//        //创建文件夹
//        [mgr createFileAtPath:picPath contents:nil attributes:nil];
//    }
//
//    dispatch_async(dispatch_queue_create("mt_ftp_download", 0), ^{
//        FMServer *server = [FMServer anonymousServerWithDestination:urlstring];
//        FTPManager *ftpMgr = [[FTPManager alloc] init];
//
//        NSString *picPath2 = [caches stringByAppendingPathComponent:@"MtPicCaches"];
//        NSURL *url = [NSURL URLWithString:picPath2];
//        if (YES == [ftpMgr downloadFile:fileName toDirectory:url fromServer:server]){
//             dispatch_sync(dispatch_get_main_queue(), ^{
//                 self.image = [UIImage imageWithContentsOfFile:picPath];
//             });
//        }else{
//            NSLog(@"下载图片失败")
//        }
//    });
//    
//    
//
//}

@end
