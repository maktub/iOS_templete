//
//  MTSelectRoot.m
//  Recruitment
//
//  Created by Maktub on 03/04/2017.
//  Copyright © 2017 Maktub. All rights reserved.
//

#import "MTSelectRoot.h"


@implementation MTSelectRoot
static NSString  * const VersionKey = @"REBundleVersion";

+(void)mt_chooseRootViewController:(UIWindow *)window{
    
    //获取当前版本号
    NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSLog(@"当前版本号为%@",currentVersion);
    
    //获取最新的版本号
    NSString *lastVersion = [[NSUserDefaults standardUserDefaults] objectForKey:VersionKey];
    NSLog(@"最新版本号为%@",lastVersion);
    
    //判断版本号,设置根控制器
    //    [currentVersion isEqualToString:lastVersion]
    if (true) {
        
        //是否已经登录
        if ([MTUserCenter sharedInstance].islogin) {
            MTTabBarController *vc = [[MTTabBarController alloc] init];
//            vc.selectedIndex = 1;
            window.rootViewController = vc;
        }else{
            UIViewController *vc = [[UIViewController alloc] init];
            MTNavigationController *nav = [[MTNavigationController alloc] initWithRootViewController:vc];
            [nav setNavigationBarHidden:YES animated:YES];
            window.rootViewController = nav;
        }
        
    }
    else{
//        UIViewController *vc = [[UIViewController alloc] init];
//        window.rootViewController = vc;
        
        //保存最新版本号
//        [[NSUserDefaults standardUserDefaults] setObject:currentVersion forKey:VersionKey];
    }
    
}

@end
