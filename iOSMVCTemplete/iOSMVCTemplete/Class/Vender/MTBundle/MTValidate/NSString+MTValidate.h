//
//  NSString+MTValidate.h
//  onemoney
//
//  Created by Maktub on 16/3/29.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MTValidate)

//邮箱
- (BOOL) mt_isEmail;
//手机
- (BOOL) mt_isPhone;
//密码
- (BOOL) mt_isPassword;
//校验码
- (BOOL) mt_isCode;
//用户名
- (BOOL) mt_isNickName;
//年龄
- (BOOL) mt_isAge;

- (BOOL) mt_isNumber;

- (BOOL)mt_isIDCard;

- (BOOL)mt_isBankCard;

@end
