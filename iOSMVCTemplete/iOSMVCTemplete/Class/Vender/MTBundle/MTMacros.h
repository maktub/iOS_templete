//
//  MTMacros.h
//  YingCaa
//
//  Created by interest on 16/6/4.
//  Copyright © 2016年 interest. All rights reserved.
//

#ifndef MTMacros_h
#define MTMacros_h

//默认屏幕宽度和高度
#define MTScreenWidth [UIScreen mainScreen].bounds.size.width
#define MTScreenHeight [UIScreen mainScreen].bounds.size.height

//设置语言
#define MTLocalizedString(key,tabl) [NSString stringWithFormat:@"%@", [[NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:YCLangKey]] ofType:@"lproj"]] localizedStringForKey:(key) value:nil table:(tabl)]]

//获取主窗口
#define MTKeyWindows [UIApplication sharedApplication].keyWindow

//设置debug输出
#ifdef DEBUG
#define NSLog(format,...) NSLog((@"[%@][%s][%@]" format), [[[NSString stringWithUTF8String:__FILE__] componentsSeparatedByString:@"项目"] lastObject], __FUNCTION__,[NSThread currentThread], ##__VA_ARGS__);
#else
#define NSLog(format,...)
#endif

//判断是否为空
#define MTNULLNSString(string)  [string isEqual:[NSNull null]] || nil == string ? @"":string

//statusbar
#define MTStatusBarHeight [[UIApplication sharedApplication] statusBarFrame].size.height


#endif /* MTMacros_h */
