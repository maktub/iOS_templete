//
//  MTProgressHUD.m
//  onemoney
//
//  Created by Maktub on 16/3/6.
//  Copyright © 2016年 Maktub-小明. All rights reserved.
//

#import "MTProgressHUD.h"
#import "MBProgressHUD.h"

#import "UIView+MTGetVc.h"

#import "MTMacros.h"

@implementation MTProgressHUD

+ (BOOL)mt_showHUDAddedTo:(UIView *)view animated:(BOOL)animated message:(NSString *)message{
    if (nil != view) {
        MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:view];
        if (message) {
            hud.label.font = [UIFont systemFontOfSize:14.0f];
            hud.label.text = message;
            
        }
        hud.tag = 10001;
        hud.removeFromSuperViewOnHide = YES;
        [view addSubview:hud];
        [hud showAnimated:animated];
        return YES;
    }
    return NO;
}

+ (BOOL)mt_hideHUDForView:(UIView *)view animated:(BOOL)animated {
    if (nil != view) {
        MBProgressHUD *hud;
        NSEnumerator *subviewsEnum = [view.subviews reverseObjectEnumerator];
        for (UIView *subview in subviewsEnum) {
            if ([subview isKindOfClass:[MBProgressHUD class]] && subview.tag / 10000 == 1 && subview.tag % 10000 == 1) {
                
                hud = (MBProgressHUD *)subview;
            }
        }
        if (nil != hud) {
            hud.tag = 10000;
            hud.removeFromSuperViewOnHide = YES;
            [hud hideAnimated:animated];
//            
//            MBProgressHUD *hud = [MBProgressHUD HUDForView:view];
//            hud.removeFromSuperViewOnHide = YES;
//            [hud hideAnimated:animated];
            return YES;
        }
    }
    return NO;
}

+ (BOOL)mt_showMessage:(NSString *)message animated:(BOOL)animated{

    return [self mt_showMessage:message animated:animated complete:nil];
}

+ (BOOL)mt_showMessage:(NSString *)message animated:(BOOL)animated complete:(void(^)(void))block{
    
    if (message) {
        MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:MTKeyWindows];
        hud.label.font = [UIFont systemFontOfSize:14.0f];
        hud.label.text = message;
        hud.mode = MBProgressHUDModeText;
        hud.removeFromSuperViewOnHide = YES;
        [MTKeyWindows addSubview:hud];
        [hud showAnimated:animated];
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [hud hideAnimated:YES];
            if (nil != block) {
                block();
            }
        });
        return YES;
    }
    return NO;
}

@end
