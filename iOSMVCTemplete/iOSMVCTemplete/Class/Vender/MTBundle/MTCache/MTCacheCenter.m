//
//  MTCacheCenter.m
//  YingCaa
//
//  Created by interest on 16/4/29.
//  Copyright © 2016年 interest. All rights reserved.
//

#import "MTCacheCenter.h"

//model
#import "MTUserModel.h"

@implementation MTCacheCenter

+ (instancetype)sharedInstance{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
        
    });
    return sharedInstance;
}

- (void)yc_cacheWithUser:(MTUserModel *)user{
    NSString *cachesDir = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString *plistPath = [cachesDir stringByAppendingPathComponent:@"CacheCenter/user.plist"];
    [NSKeyedArchiver archiveRootObject:user toFile:plistPath];
}

- (MTUserModel *)yc_getCacheUser{
    NSString *cachesDir = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString *plistPath = [cachesDir stringByAppendingPathComponent:@"CacheCenter/user.plist"];
    NSLog(@"%@",plistPath);
    NSFileManager *mgr = [NSFileManager defaultManager];
    MTUserModel *user = nil;
    if ([mgr fileExistsAtPath:plistPath]) {
        if ([[NSKeyedUnarchiver unarchiveObjectWithFile:plistPath] isKindOfClass:[MTUserModel class]]) {
            user = [NSKeyedUnarchiver unarchiveObjectWithFile:plistPath];
        }
    }else{
        //创建文件夹
        [mgr createDirectoryAtPath:[cachesDir stringByAppendingPathComponent:@"CacheCenter"]  withIntermediateDirectories:YES attributes:nil error:nil];
        //创建文件夹
        [mgr createFileAtPath:plistPath contents:nil attributes:nil];
    }
    
    return user;
}



@end
