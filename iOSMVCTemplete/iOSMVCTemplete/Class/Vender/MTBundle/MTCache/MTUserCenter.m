//
//  MTUserCenter.m
//  YingCaa
//
//  Created by interest on 16/4/29.
//  Copyright © 2016年 interest. All rights reserved.
//

#import "MTUserCenter.h"

@interface MTUserCenter()


@end

@implementation MTUserCenter

static MTUserCenter *sharedInstance;

+ (instancetype)sharedInstance{
    if (nil == sharedInstance) {
        
        sharedInstance = [[self alloc] init];
        sharedInstance->_user = [[MTCacheCenter sharedInstance] yc_getCacheUser];
    }
    
    return sharedInstance;
}

-(MTUserModel *)user{
    if (nil == _user) {
        _user = [[MTUserModel alloc] init];
    }
    return _user;
}

+ (void)tearDown{
    
    sharedInstance = nil;
    [[MTCacheCenter sharedInstance] yc_cacheWithUser:sharedInstance.user];
}
+(void)save{
    [[MTCacheCenter sharedInstance] yc_cacheWithUser:sharedInstance.user];
}

-(BOOL)islogin{
    
    if (nil == sharedInstance.user.accessToken || [sharedInstance.user.accessToken isEqualToString:@""]) {
        
        return NO;
    }else{
        
        return YES;
    }
}

+ (void)upDataUserWithModel:(NSObject *)model{
    if (nil == [MTUserCenter sharedInstance].user) {
        [MTUserCenter sharedInstance].user = [[MTUserModel alloc] init];
    }
    [[MTUserCenter sharedInstance].user mt_setPropertyWithModel:model];
//    NSLog(@"%@,%@",[MTUserCenter sharedInstance].user.mt_keyValues,model.mt_keyValues);
    [[MTCacheCenter sharedInstance] yc_cacheWithUser:sharedInstance.user];
}
@end
