//
//  MTUserModel.h
//  YingCaa
//
//  Created by interest on 16/4/29.
//  Copyright © 2016年 interest. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface MTUserModel : NSObject

@property (nonatomic, copy) NSString *userPassword;

@property (nonatomic, assign) NSInteger uMsId;

@property (nonatomic, copy) NSString *accessToken;
@property (nonatomic, copy) NSString *userNickname;


@property (nonatomic, copy) NSString *userIcon;

@property (nonatomic, copy) NSString *userLinkphone;

@property (nonatomic, copy) NSString *userSerialNumber;

@property (nonatomic, assign) NSInteger userId;

@property (nonatomic, copy) NSString *userIdentityCard;

@property (nonatomic, copy) NSString *userName;

@property (nonatomic, copy) NSString *userAccount;

@property (nonatomic, strong) NSArray *uroomList;

@property (nonatomic, copy) NSString *userSex;



@property (nonatomic, assign) NSInteger identifyClassify;

@property (nonatomic, copy) NSString *registerName;

@property (nonatomic, assign) NSInteger pRoomId;

@property (nonatomic, copy) NSString *registerTelephone;

@property (nonatomic, assign) NSInteger failureInfo;

@property (nonatomic, copy) NSString *applyTime;

@property (nonatomic, assign) NSInteger checkStatus;

@property (nonatomic, copy) NSString *managerInfo;

@property (nonatomic, assign) NSInteger pManagerId;

@property (nonatomic, assign) NSInteger pUserId;

@property (nonatomic, assign) NSInteger pBuildId;

@property (nonatomic, assign) NSInteger pOwnerId;

@property (nonatomic, copy) NSString *buildInfo;

@property (nonatomic, copy) NSString *managerName;

@property (nonatomic, copy) NSString *roomInfo;

@property (nonatomic, assign) BOOL privilege;


@end
